package navneet.com.waywedesigntest;

/**
 * Created by Navneet Krishna on 03/08/19.
 */
public class ApplianceCategory {
    private String categoryID;
    private String categoryName;

    public ApplianceCategory(String categoryID, String categoryName) {
        this.categoryID = categoryID;
        this.categoryName = categoryName;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
