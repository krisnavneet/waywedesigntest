package navneet.com.waywedesigntest;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Navneet Krishna on 03/08/19.
 */
@Entity(tableName = "appliances_table")
public class Appliances {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_new")
    private int id_new;

    @ColumnInfo(name = "id")
    private String app_id;

    @ColumnInfo(name = "appliance_name")
    private String applianceName;

    private String status;

    public Appliances(String applianceName, String status, @NonNull String app_id) {
        this.applianceName = applianceName;
        this.status = status;
        this.app_id = app_id;
    }

    public void setId_new(int id_new) {
        this.id_new = id_new;
    }

    @NonNull
    public int getId_new() {
        return id_new;
    }

    public String getApplianceName() {
        return applianceName;
    }

    public String getStatus() {
        return status;
    }

    @NonNull
    public String getApp_id() {
        return app_id;
    }
}
