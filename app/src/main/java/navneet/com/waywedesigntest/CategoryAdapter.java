package navneet.com.waywedesigntest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Navneet Krishna on 03/08/19.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private ArrayList<ApplianceCategory> categories;
    private Context mContext;
    private int rowIndex=-1;
    private CategoryInterface categoryInterface;

    public CategoryAdapter(ArrayList<ApplianceCategory> categories,Context mContext,CategoryInterface categoryInterface){
        this.categories=categories;
        this.mContext=mContext;
        this.categoryInterface=categoryInterface;
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_list_item,viewGroup,false);
        return new CategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.category_name.setText(categories.get(i).getCategoryName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    rowIndex = i;
                    notifyDataSetChanged();
                    categoryInterface.onCategoryItemSelected(categories.get(i).getCategoryName());
            }
        });


        if(rowIndex==i) {
            viewHolder.category_name.setTextColor(mContext.getResources().getColor(R.color.green_sel));
        } else {
            viewHolder.category_name.setTextColor(mContext.getResources().getColor(R.color.text_shift));
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView category_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            category_name=(TextView)itemView.findViewById(R.id.category_name);
        }
    }
}
