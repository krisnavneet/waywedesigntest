package navneet.com.waywedesigntest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Navneet Krishna on 03/08/19.
 */
public class AppliancesAdapter extends RecyclerView.Adapter<AppliancesAdapter.ViewHolder> implements Filterable {

    private ArrayList<Appliances> appliances;
    private ArrayList<Appliances> mArrayList;
    private Context mContext;

    public AppliancesAdapter(ArrayList<Appliances> appliances,Context mContext){
        this.appliances=appliances;
        this.mContext=mContext;
        this.mArrayList=appliances;
    }

    @NonNull
    @Override
    public AppliancesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.appliance_list_item,viewGroup,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull AppliancesAdapter.ViewHolder viewHolder, int i) {
        viewHolder.appliance_name.setText(appliances.get(i).getApplianceName());
        viewHolder.status.setText(appliances.get(i).getStatus());
        if (appliances.get(i).getApp_id().equals("ac")) {
            viewHolder.image.setImageResource(R.drawable.air_conditioner);
        } else if (appliances.get(i).getApp_id().equals("sl")) {
            viewHolder.image.setImageResource(R.drawable.bulb);
        } else if (appliances.get(i).getApp_id().equals("sf")) {
            viewHolder.image.setImageResource(R.drawable.fan);
        } else {
            viewHolder.image.setImageResource(R.drawable.air_conditioner);
        }
    }

    public void setAppliances(ArrayList<Appliances> appliance_list) {
        appliances=appliance_list;
        mArrayList=appliance_list;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    appliances = mArrayList;
                } else {

                    ArrayList<Appliances> filteredList = new ArrayList<>();

                    for (Appliances appliance : mArrayList) {

                        if (appliance.getApp_id().startsWith(charString)) {

                            filteredList.add(appliance);
                        }
                    }

                    appliances = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = appliances;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                appliances = (ArrayList<Appliances>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return appliances!=null?appliances.size():0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView appliance_name,status;
        private ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            appliance_name=(TextView)itemView.findViewById(R.id.appliance_name);
            status=(TextView)itemView.findViewById(R.id.status);
            image=(ImageView)itemView.findViewById(R.id.appliance_image);
        }
    }
}
