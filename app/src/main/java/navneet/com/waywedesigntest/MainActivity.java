package navneet.com.waywedesigntest;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import navneet.com.waywedesigntest.room.ApplianceViewModel;

public class MainActivity extends AppCompatActivity {

    private RecyclerView appliances_list;
    private ArrayList<Appliances> appliances=new ArrayList<>();
    private AppliancesAdapter appliancesAdapter;
    private TextView see_all;
    private ApplianceViewModel mApplianceViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (InitApplication.getInstance().isNightModeEnabled()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        setContentView(R.layout.activity_main);

        mApplianceViewModel = ViewModelProviders.of(this).get(ApplianceViewModel.class);


        appliances_list=(RecyclerView)findViewById(R.id.appliances_list);
        appliances_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        populateAppliances();

        see_all=(TextView)findViewById(R.id.see_all);

        SwitchCompat switchCompat = findViewById(R.id.switchCompat);
//        Button button = findViewById(R.id.button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new AlertDialog.Builder(MainActivity.this, R.style.MyDialog)
//                        .setTitle("Title")
//                        .setMessage("Message")
//                        .show();
//            }
//        });

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
            switchCompat.setChecked(true);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    InitApplication.getInstance().setIsNightModeEnabled(true);
                    Intent intent = getIntent();
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    startActivity(intent);

                } else {
                    InitApplication.getInstance().setIsNightModeEnabled(false);
                    Intent intent = getIntent();
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    startActivity(intent);
                }


            }
        });

        see_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,DevicesActivity.class);
                startActivityForResult(intent,0);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });

    }

    private void populateAppliances() {
        Appliances appliance=new Appliances("Air Conditioner","On for last 3 hrs","ac");
        Appliances appliance1=new Appliances("Smart Light","On for last 5 hrs","sl");
        Appliances appliance2=new Appliances("Smart Fan","On for last 1 hrs","sf");
        Appliances appliance3=new Appliances("Smart TV","On for last 2 hrs","stv");
        appliances.add(appliance);
        appliances.add(appliance1);
        appliances.add(appliance2);
        appliances.add(appliance3);
        appliancesAdapter=new AppliancesAdapter(appliances,this);
        appliances_list.setAdapter(appliancesAdapter);
    }

}
