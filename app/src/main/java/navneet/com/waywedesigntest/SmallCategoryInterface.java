package navneet.com.waywedesigntest;

/**
 * Created by Navneet Krishna on 04/08/19.
 */
public interface SmallCategoryInterface {
    void onCategorySelected(String category);
}
