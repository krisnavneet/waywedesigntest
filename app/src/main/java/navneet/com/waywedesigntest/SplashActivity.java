package navneet.com.waywedesigntest;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            public void run() {

                /* Create an intent that will start the main activity. */
                Intent mainIntent = new Intent(SplashActivity.this,
                        MainActivity.class);
                //SplashScreen.this.startActivity(mainIntent);
                startActivity(mainIntent);
                /* Finish splash activity so user cant go back to it. */
                SplashActivity.this.finish();

                     /* Apply our splash exit (fade out) and main
                        entry (fade in) animation transitions. */
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        }, SPLASH_DISPLAY_TIME);
    }
}
