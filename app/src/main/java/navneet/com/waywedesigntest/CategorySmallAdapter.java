package navneet.com.waywedesigntest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Navneet Krishna on 03/08/19.
 */
public class CategorySmallAdapter extends RecyclerView.Adapter<CategorySmallAdapter.ViewHolder>{

    private ArrayList<ApplianceCategory> categories;
    private ArrayList<ApplianceCategory> mArrayList;
    private Context mContext;
    private int rowIndex=-1;
    private SmallCategoryInterface smallCategoryInterface;

    public CategorySmallAdapter(ArrayList<ApplianceCategory> categories, Context mContext,SmallCategoryInterface smallCategoryInterface){
        this.categories=categories;
        this.mContext=mContext;
        this.smallCategoryInterface=smallCategoryInterface;
    }

    @NonNull
    @Override
    public CategorySmallAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_small_list_item,viewGroup,false);
        return new CategorySmallAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategorySmallAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.category_name.setText(categories.get(i).getCategoryName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    smallCategoryInterface.onCategorySelected(categories.get(i).getCategoryName());
                    rowIndex = i;
                    notifyDataSetChanged();
            }
        });


        if(rowIndex==i) {
            viewHolder.category_name.setTextColor(mContext.getResources().getColor(R.color.green_sel));
        } else {
            viewHolder.category_name.setTextColor(mContext.getResources().getColor(R.color.text_shift));
        }
    }


    @Override
    public int getItemCount() {
        return categories.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView category_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            category_name=(TextView)itemView.findViewById(R.id.category_name);
        }
    }
}
