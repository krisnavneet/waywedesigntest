package navneet.com.waywedesigntest.room;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import navneet.com.waywedesigntest.Appliances;

/**
 * Created by Navneet Krishna on 13/07/19.
 */
public class ApplianceRepository {

    private ApplianceDao mApplianceDao;
    private LiveData<List<Appliances>> mAllAppliances;

    ApplianceRepository(Application application) {
        ApplianceRoomDatabase db = ApplianceRoomDatabase.getDatabase(application);
        mApplianceDao = db.issueDao();
        mAllAppliances = mApplianceDao.getAllIssues();
    }

    LiveData<List<Appliances>> getAllAppliances() {
        return mAllAppliances;
    }


    public void insert (Appliances appliances) {
        new insertAsyncTask(mApplianceDao).execute(appliances);
    }

    public void update (Appliances appliances) {
        new updateAsyncTask(mApplianceDao).execute(appliances);
    }

    public void delete (Appliances appliances) {
        new deleteAsyncTask(mApplianceDao).execute(appliances);
    }

    private static class insertAsyncTask extends AsyncTask<Appliances, Void, Void> {

        private ApplianceDao mAsyncTaskDao;

        insertAsyncTask(ApplianceDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Appliances... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Appliances, Void, Void> {

        private ApplianceDao mAsyncTaskDao;

        updateAsyncTask(ApplianceDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Appliances... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Appliances, Void, Void> {

        private ApplianceDao mAsyncTaskDao;

        deleteAsyncTask(ApplianceDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Appliances... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }
}

