package navneet.com.waywedesigntest.room;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import navneet.com.waywedesigntest.Appliances;

/**
 * Created by Navneet Krishna on 13/07/19.
 */
public class ApplianceViewModel extends AndroidViewModel {

    private ApplianceRepository mRepository;

    private LiveData<List<Appliances>> mAllAppliances;

    public ApplianceViewModel(Application application) {
        super(application);
        mRepository = new ApplianceRepository(application);
        mAllAppliances = mRepository.getAllAppliances();
    }

    public LiveData<List<Appliances>> getAllAppliances() {
        return mAllAppliances;
    }

    public void insert(Appliances appliances) {
        mRepository.insert(appliances);
    }

    public void delete(Appliances appliances) {
        mRepository.delete(appliances);
    }

    public void update(Appliances appliances) {
        mRepository.update(appliances);
    }
}
