package navneet.com.waywedesigntest.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import navneet.com.waywedesigntest.Appliances;

/**
 * Created by Navneet Krishna on 04/08/19.
 */
@Dao
public interface ApplianceDao {
    @Insert
    void insert(Appliances appliance);

    @Update
    void update(Appliances appliance);

    @Delete
    void delete(Appliances appliance);

    @Query("DELETE FROM appliances_table")
    void deleteAll();

    @Query("SELECT * FROM appliances_table where id_new=:id")
    Appliances getIssue(int id);

    @Query("SELECT * from appliances_table ORDER BY id_new ASC")
    LiveData<List<Appliances>> getAllIssues();
}
