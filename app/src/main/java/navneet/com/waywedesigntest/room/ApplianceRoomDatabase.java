package navneet.com.waywedesigntest.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import navneet.com.waywedesigntest.Appliances;

/**
 * Created by Navneet Krishna on 13/07/19.
 */
@Database(entities = {Appliances.class}, version = 1)
public abstract class ApplianceRoomDatabase extends RoomDatabase {

    public abstract ApplianceDao issueDao();

    private static volatile ApplianceRoomDatabase INSTANCE;

    static ApplianceRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ApplianceRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ApplianceRoomDatabase.class, "appliance_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
