package navneet.com.waywedesigntest;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import navneet.com.waywedesigntest.room.ApplianceViewModel;

public class DevicesActivity extends AppCompatActivity implements SmallCategoryInterface,CategoryInterface {

    private ApplianceViewModel mApplianceViewModel;
    private RecyclerView appliance_category,categories;
    private RecyclerView appliance_list;
    private ArrayList<ApplianceCategory> applianceCategories=new ArrayList<>();
    private ArrayList<Appliances> appliances=new ArrayList<>();
    private AppliancesAdapter appliancesAdapter;
    private CategoryAdapter categoryAdapter;
    private CardView add_appliance;
    private CategorySmallAdapter categorySmallAdapter;
    private String chosenCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        mApplianceViewModel = ViewModelProviders.of(this).get(ApplianceViewModel.class);

        add_appliance=(CardView) findViewById(R.id.add_appliance);
        appliance_list=(RecyclerView)findViewById(R.id.appliance_list);
        appliance_category=(RecyclerView)findViewById(R.id.appliance_category);
        appliance_category.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        appliance_list.setLayoutManager(new GridLayoutManager(this,2));

        appliancesAdapter=new AppliancesAdapter(appliances,this);
        appliance_list.setAdapter(appliancesAdapter);


        mApplianceViewModel.getAllAppliances().observe(this, new Observer<List<Appliances>>() {
            @Override
            public void onChanged(@Nullable final List<Appliances> appliances1) {
                // Update the cached copy of the appliances in the adapter.
                if (appliances1!=null) {
                    appliances=new ArrayList<Appliances>(appliances1);
                    appliancesAdapter.setAppliances(new ArrayList<Appliances>(appliances1));
                }
            }
        });

        add_appliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewDevice();
            }
        });

        populateCategories();
//        populateAppliances();
    }

    private void addNewDevice() {
        final Dialog addNewPopup=new Dialog(this,R.style.DialogTheme);
        addNewPopup.setContentView(R.layout.insert_popup_dialog);
        categories=(RecyclerView)addNewPopup.findViewById(R.id.categories);
        categories.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        ImageView close_popup=(ImageView)addNewPopup.findViewById(R.id.close_popup);
        final EditText device_name=(EditText)addNewPopup.findViewById(R.id.device_name);
        AppCompatButton save_button=(AppCompatButton)addNewPopup.findViewById(R.id.save_button);
        close_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewPopup.dismiss();
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(device_name.getText()) && chosenCat!=null) {
                    Appliances appliance = new Appliances(device_name.getText().toString(),"On for last 3 hrs",chosenCat);
                    mApplianceViewModel.insert(appliance);
                    addNewPopup.dismiss();
                }

                if (TextUtils.isEmpty(device_name.getText())) {
                    device_name.setError("Please enter name!");
                }

                if (chosenCat==null) {
                    Toast.makeText(DevicesActivity.this,"Please Choose category!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        categories.setAdapter(categorySmallAdapter);
        addNewPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        addNewPopup.show();
    }

    private void populateCategories() {
        ApplianceCategory category1=new ApplianceCategory("bd","Bed Room");
        ApplianceCategory category2=new ApplianceCategory("lr","Living Room");
        ApplianceCategory category3=new ApplianceCategory("dr","Dining Room");
        ApplianceCategory category4=new ApplianceCategory("kt","Kitchen");
        applianceCategories.add(category1);
        applianceCategories.add(category2);
        applianceCategories.add(category3);
        applianceCategories.add(category4);
        categoryAdapter=new CategoryAdapter(applianceCategories,this,this);
        categorySmallAdapter=new CategorySmallAdapter(applianceCategories,this,this);
        appliance_category.setAdapter(categoryAdapter);
    }

    private void populateAppliances() {
        Appliances appliance=new Appliances("Air Conditioner","On for last 3 hrs","ac");
        Appliances appliance1=new Appliances("Smart Light","On for last 5 hrs","sl");
        Appliances appliance2=new Appliances("Smart Fan","On for last 1 hrs","sf");
        Appliances appliance3=new Appliances("Smart TV","On for last 2 hrs","stv");
        appliances.add(appliance);
        appliances.add(appliance1);
        appliances.add(appliance2);
//        appliances.add(appliance3);
        appliancesAdapter=new AppliancesAdapter(appliances,this);
        appliance_list.setAdapter(appliancesAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
    }

    @Override
    public void onCategorySelected(String category) {
        chosenCat=category;
    }

    @Override
    public void onCategoryItemSelected(String category) {
        if (appliancesAdapter!=null)
            appliancesAdapter.getFilter().filter(category);
    }
}
